function rhs = RHS3D(U,time)
 
% function [rhsu, rhsv, rhsp] = acousticsRHS2D(u,v,p)
% Purpose  : Evaluate RHS flux in 2D acoustics TM form
 
Globals3D;
 
% Define field differences at faces
Nfld = length(U);
dU = cell(Nfld,1);
Ux = cell(Nfld,1);
Uy = cell(Nfld,1);

Nfld=13;
for fld = 1:Nfld
    dU{fld} = zeros(Nfp*Nfaces,K); dU{fld}(:) = U{fld}(vmapP)-U{fld}(vmapM);
    Ur = Dr*U{fld}; Us = Ds*U{fld}; Ut=Dt*U{fld};   
    Ux{fld} = rx.*Ur + sx.*Us + tx.*Ut +.5*LIFT*(Fscale.*dU{fld}.*nx);
    Uy{fld} = ry.*Ur + sy.*Us + ty.*Ut +.5*LIFT*(Fscale.*dU{fld}.*nx);
    Uz{fld} = rz.*Ur + sz.*Us + tz.*Ut +.5*LIFT*(Fscale.*dU{fld}.*nx);
end


rhs = cell(Nfld,1);
rhs{1} =  Ux{8};
rhs{2} =  Uy{9};
rhs{3} =  Uz{10};
rhs{4} =  Uz{9} + Uy{10};
rhs{5} =  Uz{8} + Ux{10};
rhs{6} =  Uy{8} + Ux{9};
rhs{7} = -Ux{11} -Uy{12} -Uz{13};
rhs{8} =  Ux{1} + Uy{6} + Uz{5};
rhs{9} = Ux{6} + Uy{2} + Uz{4};
rhs{10} = Ux{5} + Uy{4} + Uz{3};
rhs{11} = -Ux{7};
rhs{12} = -Uy{7};
rhs{13} = -Uz{8};


 
% penalty fluxes: An^2 * [[Q]] = An* (An*[[Q]])
ff{1} =  nx.*dU{8};
ff{2} =  ny.*dU{9};
ff{3} =  -nz.*dU{10};
ff{4} =  nz.*dU{9} + ny.*dU{10};
ff{5} =  nz.*dU{8} + nx.*dU{10};
ff{6} =  ny.*dU{8} + nx.*dU{9};
ff{7} =  -nx.*dU{11} -ny.*dU{12} - nz.*dU{13};
ff{8} =  nx.*dU{1} + ny.*dU{6} + nz.*dU{5};
ff{9} =  nx.*dU{6} + ny.*dU{2} + nz.*dU{4};
f{10} = nx.*dU{5} + ny.*dU{4} + nz.*dU{3};
f{11} = -nx.*dU{7};
f{12} = -ny.*dU{7};
f{13} = -nz.*dU{8};




% apply An again to An.*[[Q]]
f{1} =  nx.*ff{8};
f{2} =  ny.*ff{9};
f{3} =  -nz.*ff{10};
f{4} =  nz.*ff{9} + ny.*ff{10};
f{5} =  nz.*ff{8} + nx.*ff{10};
f{6} =  ny.*ff{8} + nx.*ff{9};
f{7} =  -nx.*ff{11} -ny.*ff{12} - nz.*ff{13};
f{8} =  nx.*ff{1} + ny.*ff{6} + nz.*ff{5};
f{9} =  nx.*ff{6} + ny.*ff{2} + nz.*ff{4};
f{10} = nx.*ff{5} + ny.*ff{4} + nz.*ff{3};
f{11} = -nx.*ff{7};
f{12} = -ny.*ff{7};
f{13} = -nz.*ff{8};
 
tau = 1.0; % todo: choose this more carefully!
global rick ptsrc ;
global Vq Pq;
 
rr = cell(Nfld,1);
for fld = 1:Nfld    
    rr{fld} = rhs{fld} - tau*.5*LIFT*(Fscale.*f{fld});
    rr{fld} = Vq*rr{fld};
end
 
global invA0
rhs = cell(Nfld,1);
for ii = 1:Nfld
    rhs{ii} = zeros(Np,K);
    for jj = 1:Nfld
        rhs{ii} = rhs{ii} -  Pq*(invA0{ii,jj}.*rr{jj}); % negative sign
    end
end
 rhs{3}=rhs{3}+ rick(time)*ptsrc;
 rhs{1}=rhs{1} + rick(time)*ptsrc;
end