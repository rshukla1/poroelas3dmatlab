% function Biot2D
 
clear all -globals
Globals3D

%PoroelasticGlobals3D 
N =1;
FinalTime =2000;
[Nv, VX, VY,VZ, K, EToV]=MeshReaderGmsh3D('cube1.msh');

StartUp3D;




%%%%% params setup
 
U = cell(13,1);
for i = 1:8
    U{i} = zeros(Np,K);
end
% U{1} = exp(-20^2*(x.^2+y.^2));




global invA0
invA0=q_matrix();


 
%%%%%%
 
time = 0;
 
% Runge-Kutta residual storage
res = cell(13,1);
for fld = 1:13
    res{fld} = zeros(Np,K);
end
 
% compute time step size
CN = (N+1)*(N+3)/3.0; % guessing...
dt = 0.25/(CN*max(Fscale(:)));
CFL=1;
dt=CFL*dt;
 keyboard;
tstep=0;
Ntsteps =ceil(FinalTime/dt);
while (time<FinalTime)
    tstep=tstep+1;
    if(time+dt>FinalTime), dt = FinalTime-time; end
    
    for INTRK = 1:5
        
        timelocal = time + rk4c(INTRK)*dt;
        
        rhs = RHS2D(U,timelocal);
        
        % initiate and increment Runge-Kutta residuals
        for fld = 1:length(U)
            res{fld} = rk4a(INTRK)*res{fld} + dt*rhs{fld};
            U{fld} = U{fld} + rk4b(INTRK) * res{fld};
        end                
        
    end

    time = time+dt; 
    
end
 
