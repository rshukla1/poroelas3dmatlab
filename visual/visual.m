% x=load('x.txt');
% y=load('y.txt');
% z=load('z.txt');
% p=load('Sigxx.txt');

Globals3D
N=1;
nn=18;
VX=linspace(-2,2, nn+1);
VZ=linspace(-2,2, nn+1);

[VX,VY,VZ]=meshgrid(VX,VX,VZ);
VX=VX(:);
VY=VY(:);
VZ=VZ(:);
Nv=size(VX,1);
etov=DelaunayTri(VX,VY,VZ);
VX=VX';
VY=VY';
VZ=VZ';
K=size(etov,1);
EToV=zeros(K,4);
EToV=etov(:,:);
StartUp3D;
%p=exp(-20*(x.^2 +y.^2 + z.^2));
p=load('Sigxx.txt');
[pNp,Nele]=size(x);
Nplot=1;

tt=tetsubele(Nplot);
Nsele=size(tt,1);
tt=tt';

scalname={'Sigxx'};
Nscal=length(scalname);
vectname={};
Nvect=length(vectname);

vis_Nnod=Nele*pNp;
vis_Nele=Nele*Nsele;

outfile='Sigxx';

fid=vtuxml_head(outfile,vis_Nnod,vis_Nele,scalname,vectname);
fclose(fid);

fid=fopen([outfile,'.vtu'],'a');
vtuxml_xyz(fid,[x(:),y(:),z(:)]);
disp('coordinate done')
%clear x y z;

t=pNp*ones(4*Nsele,1)*(0:Nele-1);
t=t+tt(:)*ones(1,Nele);
t=reshape(t,4,vis_Nele)-1;

vtuxml_conn(fid,t);
disp('connection done')
%clear t;

vtuxml_var(fid,p(:));
disp('scalar done')
%clear p;

vtuxml_end(fid);

