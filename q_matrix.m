function [Q]=q_matrix()
Ks=40.0*0.01;
RHOS=2.500;
C11=36*0.01;
C22=C11;
C12=12*0.01;
C13=12*0.01;
C33=36*0.01;
C55=12*0.01;
C44=C55;
C66=(C11-C12)/2;
C23=C13;
PHI=0.2;
KAPPA1=600*10^-11;
KAPPA2=KAPPA1;
KAPPA3=600*10^-11;
T1=2;
T2=T1;
T3=3.6;
Kf=2.5*0.01;
RHOF=1.040;
ETA=0*10^-8;
ALPHA1=1.0-((C11+ C12+ C13)/(3*Ks));
ALPHA2=ALPHA1;
ALPHA3=1.0-((2*C13 + C33)/(3*Ks));
D=Ks*(1.0+PHI*((Ks/Kf)-1.0));
M=(Ks*Ks)/(D-(2*C11+C33+2.0*C12+4.0*C13)/9.0);
RHO=(1.0-PHI)*RHOS + PHI*RHOF;
m1=T1*RHO/PHI;
m2=T2*RHO/PHI;
m3=T3*RHO/PHI;

C=[C11 C12 C13 0 0 0;
   C12 C11 C13 0 0 0;
   C13 C13 C33 0 0 0; 
   0 0 0 C55 0 0;
   0 0 0 0 C55 0;
   0 0 0 0 0 (C11-C12)/2;
];

inC=inv(C);
alpha=[ALPHA1;ALPHA2;ALPHA3;0;0;0];
S11=inC;
S12=S11*alpha;
S21=alpha'*inC;
S22=(1/M) + alpha'*inC*alpha;
Es=[S11 S12;
    S21 S22];

Ev=[RHO 0 0 RHOF 0 0;
    0 RHO 0 0 RHOF 0;
    0 0 RHO 0 0 RHOF;
    RHOF 0 0 m1 0 0;
    0 RHOF 0 0 m2 0;
    0 0 RHOF 0 0 m3;
    
];

Q1=blkdiag(Es,Ev);
Q=inv(Q1);

